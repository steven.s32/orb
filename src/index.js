const readline = require('readline')
const fs = require('fs')
const os = require('os')
const path = require('path')
const { promisifyCallbackFn } = require('../../common')

/*
const state = {
    oto: {
        product: {
            infrastructure: {
                persistence: {
                    decide: {
                        _data: 'firebase? only temporary..'
                    }
                },
                notifications: {
                    decide: {
                        _data: 'websockets? polling?'
                    }
                },
                security: {
                    decide: {
                        _data: 'web crypto/crypto.subtle'
                    }
                }
            },
            ux: {
                design: {},
                'basic design': {
                    _comments: [
                        {author: 'steven', content: 'started', time: '2019-04-29T12:00:00'}
                    ]
                },
                'mock api': {}
            }
        },
        team: {
            steven: {},
            growth: {},
            design: {},
            security: {}
        }
    }
}
*/

const formatNode = node => {
    const keys = Object.keys(node)

    //return keys.filter(k => k[0] !== '_').join(', ')
    return keys.map(k => k[0] === '_' ? `${k.slice(1)}: ${node[k]}` : k).join(', ')
}

const buildProcessorAndState = (root, question) => {
    const nodeStack = []
    let currentParent = root

    incr = 0

    const noop = () => void 0
    const commands = {
        raise: noop,
        finish: noop,
        remove: title => {
            delete currentParent[title]
        },
        add: title => {
            if (title) {
                const item = {}
                const parent = currentParent

                parent[title] = item
            } else {
                return 'Need a name'
            }
        },
        go: title => {
            nodeStack.push(currentParent)
            currentParent = currentParent[title]
        },
        up: title => {
            if (nodeStack.length) {
                currentParent = nodeStack.pop()
            } else {
                currentParent = root
                return 'At root'
            }
        },
        write: title => question('description: ').then(description => {
            currentParent[title]._description = description
        }),
        show: () => formatNode(currentParent),
        all: () => root,
        exit: () => -1
    }

    commands.a = commands.add
    commands.delete = commands.remove
    commands.d = commands.remove
    commands.u = commands.up
    commands.g = commands.go
    commands.s = commands.show
    commands.w = commands.write

    const processCommand = (com, title, ...instructions) => {
        if (com in commands) {
            const response = commands[com](title, ...instructions)

            if (!response) {
                return commands.show()
            }

            return response
        } else {
            return `Didn't understand that ${com}`
        }
    }

    return {
        processCommand,
        json: () => JSON.stringify(root)
    }
}

const readAsJson = fileName => {
    try {
        const fileString = fs.readFileSync(fileName, 'utf8')

        return JSON.parse(fileString)
    } catch (e) {
        console.error(e)
        return {}
    }
}

const replaceWithHomeDir = filePath => {
    if (filePath[0] === '~') {
        return path.join(os.homedir(), filePath.slice(1))
    } else {
        return filePath
    }
}

const getDataRoot = () => {
    const dataRoot = readAsJson(path.join(__dirname, '..', 'orb.config')).dataRoot

    if (dataRoot) {
        return replaceWithHomeDir(dataRoot)
    }

    return '.'
}

const boot = (fileName='default_orb') => {
    const dataRootPath = getDataRoot()
    const filePath = `${dataRootPath}/${fileName}`
    const treeRoot = readAsJson(filePath)

    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
        prompt: `${fileName ? fileName : ''} ~o) `
    })

    const question = promisifyCallbackFn(rl.question.bind(rl))
    const orb = buildProcessorAndState(treeRoot, question)

    rl.prompt()

    rl.on('line', line => {
        const response = orb.processCommand(...line.split(' '))

        if (response === -1) {
            rl.close()
        } else if (response instanceof Promise) {
            response.then(() => rl.prompt())
        } else {
            console.log(response)
            rl.prompt()
        }
    }).on('close', () => {
        fs.writeFileSync(filePath, orb.json(), 'utf8')
        console.log(`Saved to ${filePath}`)
    })
}

boot(process.argv[2])
